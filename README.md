# Cert-Manager/LetsEncrypt

## Description
Helm Release template to deploy cert-manager

## Table of Contents
- [Installation](#installation)
- [Usage](#usage)


## Installation

Dependencies:
- helm chart [flux-helm-repos](https://gitlab.com/itoutposts/helm-charts/-/tree/main/charts/fluxcd-helm-repos?ref_type=heads)

```yaml
# GitRepository
---
apiVersion: source.toolkit.fluxcd.io/v1
kind: GitRepository
metadata:
  name: cert-manager
  namespace: flux-system
spec:
  interval: 1m0s
  ref:
    branch: main
  secretRef:
    name: flux-system
  url: ssh://git@gitlab.com/itoutposts/hub/security/cert-manager.git

# Kustomization
## We can override any values via kustomization
---
apiVersion: kustomize.toolkit.fluxcd.io/v1
kind: Kustomization
metadata:
  name: cert-manager
  namespace: flux-system
spec:
  interval: 10m0s
  prune: true
  sourceRef:
    kind: GitRepository
    name: cert-manager
  patches:
    - target:
        kind: HelmRelease
        name: cert-manager
        version: v2beta1
        group: helm.toolkit.fluxcd.io
      patch: |-
        apiVersion: helm.toolkit.fluxcd.io/v2beta1
        kind: HelmRelease
        metadata:
          name: cert-manager
          namespace: flux-system
        spec:
          values:
            nameOverride: cert-manager
```

## Usage

You can check all parameters in a source helm [chart](https://github.com/cert-manager/cert-manager)


